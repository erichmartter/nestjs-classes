import { ObjectType, Field, Int } from '@nestjs/graphql';
import { BaseEntity } from 'src/modules/bases/entities/base.entity';
import { ContentsModule } from 'src/modules/contents/contents.module';
import { Content } from 'src/modules/contents/entities/content.entity';
import { Column, Entity, OneToMany } from 'typeorm';

@Entity()
export class Lesson extends BaseEntity {
  @Column()
  description: string;

  @OneToMany(() => Content, (contents) => contents.lesson)
  contents: Content[];
}
